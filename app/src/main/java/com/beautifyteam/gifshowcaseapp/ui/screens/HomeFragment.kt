package com.beautifyteam.gifshowcaseapp.ui.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.beautifyteam.gifshowcaseapp.R
import com.beautifyteam.gifshowcaseapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding: FragmentHomeBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initButtons()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun initButtons() {
        with(binding){
            btnGiphyScreen.setOnClickListener{
                findNavController().navigate(R.id.action_homeFragment_to_giphyFragment)
            }

            btnTenorScreen.setOnClickListener{
                findNavController().navigate(R.id.action_homeFragment_to_tenorFragment)
            }
        }
    }

}