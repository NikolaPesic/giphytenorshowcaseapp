package com.beautifyteam.gifshowcaseapp.ui.screens.tenorscreen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tenor.android.core.model.impl.Result
import com.tenor.android.core.response.impl.TrendingGifResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TenorScreenViewModel : ViewModel() {

    private val tenorRepo = TenorRepo()

    private var _responseLiveData = MutableLiveData<List<Result>>()
    val responseLiveData: LiveData<List<Result>>
        get() = _responseLiveData

    fun getTrendingGifs() {
        tenorRepo.getTrendingGifs(object : Callback<TrendingGifResponse> {
            override fun onResponse(call: Call<TrendingGifResponse>, response: Response<TrendingGifResponse>) {
                if (response.isSuccessful)
                    _responseLiveData.value = response.body()?.results
            }

            override fun onFailure(call: Call<TrendingGifResponse>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
}