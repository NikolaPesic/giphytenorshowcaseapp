package com.beautifyteam.gifshowcaseapp.ui.screens

import android.app.DownloadManager
import android.content.Context.DOWNLOAD_SERVICE
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.navigation.fragment.findNavController
import com.beautifyteam.gifshowcaseapp.R
import com.beautifyteam.gifshowcaseapp.databinding.FragmentGiphyBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.bumptech.glide.request.transition.Transition
import com.facebook.drawee.backends.pipeline.Fresco
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.models.enums.RenditionType
import com.giphy.sdk.ui.GPHContentType
import com.giphy.sdk.ui.Giphy
import com.giphy.sdk.ui.views.GiphyDialogFragment
import java.io.File

class GiphyFragment : Fragment() {

    private var _binding: FragmentGiphyBinding? = null
    private val binding: FragmentGiphyBinding get() = _binding!!
    lateinit var file: File


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Fresco.initialize(requireContext())
        _binding = FragmentGiphyBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Giphy.configure(requireContext(), getString(R.string.api_key))
        file = File(requireContext().cacheDir, "my new file.gif")

        binding.btnOpenGifs.setOnClickListener {
            GiphyDialogFragment.newInstance().apply {
                gifSelectionListener = object : GiphyDialogFragment.GifSelectionListener {
                    override fun didSearchTerm(term: String) {}

                    override fun onDismissed(selectedContentType: GPHContentType) {}

                    override fun onGifSelected(media: Media, searchTerm: String?, selectedContentType: GPHContentType) {
                        binding.mvGiphyMediaView.setMedia(media, RenditionType.original)

                        val downloadManager = this@GiphyFragment.requireContext().getSystemService(DOWNLOAD_SERVICE) as DownloadManager
                        downloadManager.enqueue(DownloadManager.Request(Uri.parse(media.images.original?.gifUrl)).apply {
                            setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "my chosen gif.gif")
                        })
                    }
                }
            }.show(parentFragmentManager, "dialog")
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}