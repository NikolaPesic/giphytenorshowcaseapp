package com.beautifyteam.gifshowcaseapp.ui.screens.tenorscreen

import android.app.DownloadManager
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.beautifyteam.gifshowcaseapp.databinding.FragmentTenorBinding
import com.bumptech.glide.Glide

class TenorFragment : Fragment() {

    private var _binding: FragmentTenorBinding? = null
    private val binding: FragmentTenorBinding get() = _binding!!
    private val viewModel: TenorScreenViewModel by viewModels()
    private var chosenGifUrl = ""

    private lateinit var adapter: TrendingGifsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTenorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = TrendingGifsAdapter() {
            chosenGifUrl = it
            Glide.with(requireContext()).load(chosenGifUrl).into(binding.chosenGifIv)
        }

        binding.gridRv.layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
        binding.gridRv.adapter = adapter

        viewModel.responseLiveData.observe(viewLifecycleOwner) {
            adapter.setGifList(it)
            adapter.notifyDataSetChanged()
        }
        viewModel.getTrendingGifs()

        binding.downloadBtn.setOnClickListener {
            if (chosenGifUrl.isNotBlank())
                (requireContext().getSystemService(DOWNLOAD_SERVICE) as DownloadManager).enqueue(
                    DownloadManager.Request(
                        Uri.parse(
                            chosenGifUrl
                        )
                    ).apply {
                        setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "my chosen tenor gif.gif")
                    })
        }
    }

}