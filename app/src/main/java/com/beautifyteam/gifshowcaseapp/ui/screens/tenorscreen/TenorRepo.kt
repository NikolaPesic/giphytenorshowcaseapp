package com.beautifyteam.gifshowcaseapp.ui.screens.tenorscreen

import com.beautifyteam.gifshowcaseapp.GifShowcaseApplication
import com.tenor.android.core.constant.AspectRatioRange
import com.tenor.android.core.constant.MediaFilter
import com.tenor.android.core.network.ApiClient
import com.tenor.android.core.response.impl.TrendingGifResponse
import retrofit2.Callback

class TenorRepo {

    fun getTrendingGifs(callback: Callback<TrendingGifResponse>) {
        ApiClient.getInstance()
            .getTrending(
                ApiClient.getServiceIds(GifShowcaseApplication.INSTANCE),
                50,
                "",
                MediaFilter.BASIC,
                AspectRatioRange.STANDARD
            )?.also {
                it.enqueue(callback)
            }
    }
}