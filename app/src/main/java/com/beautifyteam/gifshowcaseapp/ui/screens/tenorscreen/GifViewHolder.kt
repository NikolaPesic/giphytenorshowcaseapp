package com.beautifyteam.gifshowcaseapp.ui.screens.tenorscreen

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.beautifyteam.gifshowcaseapp.R
import com.bumptech.glide.Glide
import com.tenor.android.core.constant.MediaCollectionFormat
import com.tenor.android.core.model.impl.Result

class GifViewHolder(view: View, onRVItemClick: (String) -> Unit) : RecyclerView.ViewHolder(view) {

    private val imagePlaceHolder: ImageView = view.findViewById(R.id.gif_placeholder_iv)
    var gifUrl = ""

    fun setData(result: Result) {
        gifUrl = result.medias[0][MediaCollectionFormat.GIF].url
        Glide.with(itemView.context).load(result.medias[0][MediaCollectionFormat.GIF_TINY].url).into(imagePlaceHolder)
    }

    init {
        itemView.setOnClickListener {
            onRVItemClick(gifUrl)
        }
    }
}
