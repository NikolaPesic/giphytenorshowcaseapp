package com.beautifyteam.gifshowcaseapp.ui.screens.tenorscreen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beautifyteam.gifshowcaseapp.R
import com.tenor.android.core.model.impl.Result

class TrendingGifsAdapter(private val onRVItemClick: (String) -> Unit) : RecyclerView.Adapter<GifViewHolder>() {

    private var itemsList = listOf<Result>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GifViewHolder {
        return GifViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.gif_card, parent, false), onRVItemClick)
    }

    override fun onBindViewHolder(holder: GifViewHolder, position: Int) {
        holder.setData(itemsList[position])
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun setGifList(list: List<Result>) {
        itemsList = list
    }
}