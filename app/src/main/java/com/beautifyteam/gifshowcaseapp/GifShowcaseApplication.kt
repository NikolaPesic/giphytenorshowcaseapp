package com.beautifyteam.gifshowcaseapp

import android.app.Application
import com.tenor.android.core.network.ApiClient
import com.tenor.android.core.network.ApiService
import com.tenor.android.core.network.IApiClient

class GifShowcaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        val builder = ApiService.Builder(this, IApiClient::class.java).apply {
            apiKey(getString(R.string.tenor_api_key))
        }
        ApiClient.init(this, builder)
    }

    companion object {

        lateinit var INSTANCE: GifShowcaseApplication

    }
}